package main

import (
	"fmt"
	"go101/calculator"
)

type Product struct {
	name     string
	price    float64
	category string
	discount int
}

func main() {
	// นิยามตัวแปร
	// var name string = "Praveekorn"
	// name := "Praveekorn"
	// // var age int = 25
	// age := 25
	// var score float32 = 95.8
	// score := 95.8
	// // var isPass bool = true
	// isPass := true
	// fmt.Println("My name is ", name)
	// fmt.Println("Age ", age)
	// fmt.Println("Score ", score)
	// fmt.Println("Pass Exam ", isPass)
	// fmt.Printf("My name is %v\n", name) // ใช้ printf และ %v แสดง value ของตัวแปร
	// fmt.Printf("Age = %v\n", age)
	// fmt.Printf("Type name = %T\n", name)
	// fmt.Printf("Type age = %T\n", age)
	// fmt.Printf("Type score = %T\n ", score)
	// fmt.Printf("Type isPass = %T\n ", isPass)

	// // นิยามค่าคงที่ var and const
	// var name string = "Praveekorn" // ถ้าเป็น var สามารถเปลี่ยนค่าได้
	// // const name string = "Praveekorn" // ถ้าเป็น const ไม่สามารถเปลี่ยนค่าได้
	// name = "Jojo"
	// fmt.Println("My name is ", name)

	// ตัวดำเนินการทางคณิตศาสตร์และการเปรียบเทียบ
	// number1, number2 := 10, 3
	// fmt.Println("ผลบวก = ", number1+number2)
	// fmt.Println("ผลลบ = ", number1-number2)
	// fmt.Println("ผลคูณ = ", number1*number2)
	// fmt.Println("ผลหาร = ", number1/number2)
	// fmt.Println("เศษ = ", number1%number2)
	// fmt.Println("เท่ากันหรือไม่ = ", number1 == number2)
	// fmt.Println("เท่ากันหรือไม่ = ", number1 != number2)
	// fmt.Println(number1, "มากกว่า", number2, " = ", number1 > number2)
	// fmt.Println(number1, "น้อยกว่า", number2, " = ", number1 < number2)
	// fmt.Println(number1, "มากกว่าเท่ากับ", number2, " = ", number1 >= number2)
	// fmt.Println(number1, "น้อยกว่าเท่ากับ", number2, " = ", number1 <= number2)

	// scanf %s %d %f
	// var namestudent string
	// fmt.Print("ป้อนชื่อนักเรียน = ")
	// fmt.Scanf("%s", &namestudent)
	// fmt.Println("สวัสดี = ", namestudent)
	// var scorestd int
	// fmt.Print("ป้อนคะแนนนักเรียน = ")
	// fmt.Scanf("%d", &scorestd)
	// fmt.Println("คะแนนสอบ + จิตพิสัย = ",scorestd+10)
	// var scorestdf float32
	// fmt.Print("ป้อนคะแนนนักเรียน = ")
	// fmt.Scanf("%f", &scorestdf)
	// fmt.Println("คะแนนสอบ + จิตพิสัย = ",scorestdf+10)

	//if else
	// var number int
	// fmt.Print("ป้อนตัวเลข = ")
	// fmt.Scanf("%d", &number)
	// if number == 1 {
	// 	fmt.Println("เปิดบัญชีใหม่")
	// } else if number == 2 {
	// 	fmt.Println("ฝากเงิน - ถอนเงิน")
	// } else {
	// 	fmt.Println("ข้อมูลไม่ถูกต้อง")
	// }

	// switch case
	// switch number {
	// case 1:
	// 	fmt.Println("เปิดบัญชีใหม่")
	// case 2:
	// 	fmt.Println("ฝากเงิน - ถอนเงิน")
	// default:
	// 	fmt.Println("ข้อมูลไม่ถูกต้อง")
	// }

	// Array
	// var numbers[3]int = [3]int{100,200,300}
	// numbers := [3]int{100, 200, 300}
	// numbers := [...]int{100, 200, 300,400,500,600,700,800,900,1000} // ... คือ จำนวนไม่จำกัด
	// size := len(numbers)
	// fmt.Println(numbers)
	// fmt.Println("Size of Array = ",size)

	// slices
	// numbers := []int{100, 200, 300}
	// numbers = append(numbers, 400)
	// numbers = append(numbers, 500)
	// fmt.Println(numbers[:])
	// fmt.Println(numbers[1:])
	// fmt.Println(numbers[1:3]) // index 1 - 2
	// fmt.Println(numbers[:3]) // index 0-2

	// map
	// country := map[string]string{"TH":"Thailand","JP":"Japan"}  // Key:value วิธี 1 1 บรรทัด
	// country := map[string]string{} // Key:value วิธี 2 3 บรรทัด
	// country["TH"] = "Thailand"
	// country["JP"] = "Japan"

	// value, isKey := country["EN"]
	// if isKey {
	// 	fmt.Println(value)
	// }else{
	// 	fmt.Println("No Data")
	// }

	// for loop, break, continue
	// for count := 1; count <= 10; count++ {
	// 	// fmt.Println("HELLO GO")
	// 	// fmt.Println(count)
	// 	if count == 5 {
	// 		// break
	// 		continue
	// 	}
	// 	fmt.Println(count)
	// }
	// fmt.Println("End")

	// Range
	// numbers := []int{10,20,30,40,50,60,70,80,90,100} //slice
	// for _,value := range numbers{  // _ คือ ignore
	// 	fmt.Println("Value = ",value)
	// }
	// language := map[string]string{"TH":"Thai","EN":"English","JP":"Japan"}
	// for _,value := range language {
	// 	fmt.Println("Value = ",value)
	// }

	// Function
	// myCart := getTotalCart(500, 500)
	// fmt.Println("ยอดรวมทั้งหมด = ", myCart)

	// Function return multi value
	// result, check := summation(5, 2)
	// fmt.Println("ผลรวม = ", result)
	// fmt.Println(check)

	// Variadic function
	// result := summation(10,20,30,40,50)
	// fmt.Println("ผลรวม = ", result)

	// Struct
	// product1 := Product{name: "Pen",price: 50.5,category: "tool",discount: 10}
	// product2 := Product{name: "Notebook",price: 20000,category: "Computer",discount: 30}
	// fmt.Println(product1)
	// fmt.Println(product2)
	// fmt.Println(product1.name)

	// package
	fmt.Println(calculator.Add(50, 100))
	fmt.Println(calculator.Subtract(100, 50))
}

// func getTotalCart(number1, number2 int) int {
// 	total := number1 + number2
// 	return total
// }

// func summation(number1, number2 int) (int, string) {
// 	total := number1 + number2
// 	status := ""
// 	if total%2 == 0 {
// 		status = "เลขคู่"
// 	} else {
// 		status = "เลขคี่"
// 	}
// 	return total, status
// }

// func summation(numbers ...int) int {
// 	total := 0
// 	for _, value := range numbers {
// 		total += value
// 	}
// 	return total
// }
