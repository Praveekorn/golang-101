package calculator

func Add(number1, number2 int) int { // ใช้ตัวอักษรตัวแรกตัวพิมใหญ่ เพื่อให้ package อื่นสามารถเรียกใช้ได้
	return number1 + number2
}

func Subtract(num1, num2 int) int {
	return num1 - num2
}
